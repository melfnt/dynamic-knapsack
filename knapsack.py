'''
  usage: knapsack.py input.in
  
  reads a list of items and maximum weight from input.it then solves the knapsack problem and prints the result on stdout.
  given a list of items L and a maximum weight W the knapsack problem consist in selecting a subset of L such that their sum is maximal but less or equal than W.

'''

from typing import List, Tuple, Sequence, Dict
import sys
import tqdm

from datasets_utils import parse_hc_input_dataset, print_hc_dataset

def select_items ( weights_dict: Dict[int, int], items: Sequence[int], start_weight: int ) -> List[int]:
    '''
        returns the indexes of the items to be selected to obtain exactly start_weight.
        weights_dict is a dictionary with the weights composition a computed by the knapsack() function
    '''
    out: List[int] = []
    b = start_weight
    while b > 0:
        i = weights_dict[b]
        a = items[i]
        out.append (i)
        b = b - a
        # print ("next b:", b)
        # input()
    return out

def knapsack ( items: Sequence[int], max_weight: int, checkpoint=False ) -> List[int]:
    '''
        solves the knapsack problem using dynamic programming.
        if checkpoint = True outputs the partial solutions when a new best result is found.
    '''
    weights_dict: Dict[int, int] = {0: -1}
    best = 0
    prev_best = 0
    N = len(items)
    try:
        for i,a in tqdm.tqdm(enumerate(items), total=N):
            next_weights_dict: Dict[int, int] = {}
            for b in weights_dict:
                # print ("check    weight: {}".format(b))
                next_weights_dict[b] = weights_dict[b]
                if a+b <= max_weight and not a+b in next_weights_dict:
                    next_weights_dict[a+b] = i
                    if a+b > best:
                        best = a+b
                        best_changed = True

            weights_dict = next_weights_dict
            if best != prev_best and checkpoint:
                print ("NEW BEST FOUND: {} (using {:.4f}% of the total bag capacity)".format(best, best/max_weight*100))
                print_hc_dataset ( select_items ( weights_dict, items, best ) )
                print ()
            prev_best = best
            # print ("end  item: {} reachable weights: {}".format(a, weights_dict))
            # input()
    except KeyboardInterrupt as e:
        print ("Premature exit (KeyboardInterrupt). returning the best result found so far")
    except MemoryError as e:
        print ("Premature exit (MemoryError). returning the best result found so far")
    # print ("best: {}".format(best))
    # input()
    return select_items ( weights_dict, items, prev_best )


if __name__ == "__main__":

    if len(sys.argv) < 2:
        sys.exit("usage: knapsack.py input.in")
    filename = sys.argv[1]

    items, W = parse_hc_input_dataset (filename)
    # tiny hack: since (big) datasets are quite big, sort them in decreasing order to select "heavier" items first
    N = len(items)
    items = list(reversed(items))
    to_select = knapsack(items,W,checkpoint=True)
    to_select = list(map (lambda i: N-i-1, to_select))
    print_hc_dataset (to_select)


