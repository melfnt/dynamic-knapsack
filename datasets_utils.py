'''
    utilities to handle google hash code 2020 (practice problem) files: input parsing and solution prting
'''

from typing import List, Tuple, Sequence, Dict

def parse_hc_input_dataset ( filename: str ) -> Tuple[List[int], int]:
    '''
        parses a google hash code 2020 (practice problem) input file.
        returns the list of all the items and max_weight
    '''
    with open(filename) as fin:
        max_weight, _ = [int(x) for x in fin.readline().split()]
        items = [int(x) for x in fin.readline().split()]
    return items, max_weight

def print_hc_dataset ( items: Sequence[int] ):
    '''
        prints a google hash code 2020 (practice problem) solution on stdout.
    '''
    print (len(items))
    print (" ".join(str(x) for x in items))