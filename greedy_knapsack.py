'''
  usage: greedy_knapsack.py input.in
  
  reads a list of items and maximum weight from input.it then computes an approximate solution for the knapsack problem and prints the result on stdout.
  given a list of items L and a maximum weight W the knapsack problem consist in selecting a subset of L such that their sum is maximal but less or equal than W.

'''

from typing import List, Tuple, Sequence, Dict
import sys

from datasets_utils import parse_hc_input_dataset, print_hc_dataset

def greedy_knapsack ( items: Sequence[int], max_weight: int, checkpoint=False ) -> List[int]:
    '''
        computes an approximate solution for the knapsack problem using a greedy approch.
        checkpoint parameter is ignored
    '''
    items_with_indexes = enumerate (items)
    sorted_items_with_indexes = sorted ( items_with_indexes, reverse=True, key = lambda x: x[1])
    sorted_items = list (map (lambda x: x[1], sorted_items_with_indexes))
    index_mapping = { new_index: old_index for new_index, (old_index, _) in enumerate(sorted_items_with_indexes) }
    tot = 0
    out: List[int] = []
    for i,a in enumerate(sorted_items):
        if a + tot <= max_weight:
            tot += a
            out.append ( index_mapping[i] )

    return out



if __name__ == "__main__":

    if len(sys.argv) < 2:
        sys.exit("usage: knapsack.py input.in")
    filename = sys.argv[1]

    items, W = parse_hc_input_dataset (filename)
    to_select = greedy_knapsack(items,W,checkpoint=True)
    print_hc_dataset (to_select)